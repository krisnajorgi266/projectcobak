<?php

use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SessionController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ProdukController;
use App\Http\Controllers\WilayahController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

route::get('/master', function () {

    return view('layout.master');
});

Route::get('/', [DashboardController::class, 'index']);
Route::get('/session', [SessionController::class, 'index']);
Route::post('/session/login', [SessionController::class, 'login']);
Route::post('/session/logout', [SessionController::class, 'logout']);



Route::resource('kategori', AdminController::class)->names([
    'index' => 'kategori.index',
    'create' => 'kategori.create',
    'store' => 'kategori.store',
    'show' => 'kategori.show',
    'edit' => 'kategori.edit',
    'update' => 'kategori.update',
    'destroy' => 'kategori.destroy',
]);

Route::resource('produk', ProdukController::class)->names([
    'index' => 'produk.index',
    'create' => 'produk.create',
    'store' => 'produk.store',
    'show' => 'produk.show',
    'edit' => 'produk.edit',
    'update' => 'produk.update',
    'destroy' => 'produk.destroy',
]);


Route::resource('wilayah', WilayahController::class)->names([
    'index' => 'wilayah.index',
    'create' => 'wilayah.create',
    'store' => 'wilayah.store',
    'show' => 'wilayah.show',
    'edit' => 'wilayah.edit',
    'update' => 'wilayah.update',
    'destroy' => 'wilayah.destroy',
]);
