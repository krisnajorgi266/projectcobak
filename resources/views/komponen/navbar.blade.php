

<header class="main-header-area">
        <!-- Main Header Area Start -->
        <div class="main-header header-transparent header-sticky">
            <div class="container-fluid">
                <div class="row align-items-center">
                    <div class="col-lg-2 col-xl-2 col-md-6 col-6 col-custom">
                        <div class="header-logo d-flex align-items-center">
                            <a href="index.html">
                                <img class="img-full" src="{{asset('template')}}/images/logo/logo.png" alt="Header Logo" />
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-7 d-none d-lg-flex justify-content-center col-custom">
                        <nav class="main-nav d-none d-lg-flex">
                            <ul class="nav">
                                <li>
                                    <a class="" href="index.html">
                                        <span class="menu-text">Sumatera</span>
                                        <i class="fa fa-angle-down"></i>
                                    </a>
                                    <ul class="dropdown-submenu dropdown-hover">
                                        <li><a href="contact-us.html">Aceh</a></li>
                                        <li><a href="my-account.html">Bangka</a></li>
                                        <li><a href="frequently-questions.html">Batam</a></li>
                                        <li><a href="#">Batu Raja</a></li>
                                        <li><a href="#">Bengkulu</a></li>
                                        <li><a href="#">Bukit Tinggi</a></li>
                                        <li><a href="#">Jambi</a></li>
                                        <li><a href="#">Kotabumi</a></li>
                                        <li><a href="#">Lampung</a></li>
                                        <li><a href="#">Medan</a></li>
                                        <li><a href="#">Muara Bulian</a></li>
                                        <li><a href="#">Padang</a></li>
                                        <li><a href="#">Pekanbaru</a></li>
                                        <li><a href="#">Palembang</a></li>
                                        <li><a href="#">Pematang Siantar</a></li>
                                        <li><a href="#">Riau</a></li>
                                        <li><a href="#">Tanjung Pinang</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a class="" href="index.html">
                                        <span class="menu-text"> Jawa</span>
                                        <i class="fa fa-angle-down"></i>
                                    </a>
                                    <ul class="dropdown-submenu dropdown-hover">
                                        <li><a href="#">Bandung</a></li>
                                        <li><a href="#">Banyumas</a></li>
                                        <li><a href="#">Cianjur</a></li>
                                        <li><a href="#">Cirebon</a></li>
                                        <li><a href="#">Garut</a></li>
                                        <li><a href="#">Kebumen</a></li>
                                        <li><a href="#">Kediri</a></li>
                                        <li><a href="#">Kudus</a></li>
                                        <li><a href="#">Magelang</a></li>
                                        <li><a href="#">Malang</a></li>
                                        <li><a href="#">Pekalongan</a></li>
                                        <li><a href="#">Purbalingga</a></li>
                                        <li><a href="#">Purwokerto</a></li>
                                        <li><a href="#">Rembang</a></li>
                                        <li><a href="#">Semarang</a></li>
                                        <li><a href="#">Solo</a></li>
                                        <li><a href="#">Sukabumi</a></li>
                                        <li><a href="#">Surabaya</a></li>
                                        <li><a href="#">Tasikmalaya</a></li>
                                        <li><a href="#">Tegal</a></li>
                                        <li><a href="#">Wonogiri</a></li>
                                        <li><a href="#">Wonosobo</a></li>
                                        <li><a href="#">Yogyakarta</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a class="" href="index.html">
                                        <span class="menu-text"> Kalimantan</span>
                                        <i class="fa fa-angle-down"></i>
                                    </a>
                                    <ul class="dropdown-submenu dropdown-hover">
                                        <li><a href="#">Pontianak</a></li>
                                        <li><a href="#">Palangkaraya</a></li>
                                        <li><a href="#">Samarinda</a></li>
                                        <li><a href="#">Banjarmasin</a></li>
                                        <li><a href="#">Balikpapan</a></li>

                                    </ul>
                                </li>
                                <li>
                                    <a class="" href="index.html">
                                        <span class="menu-text"> Sulawesi</span>
                                        <i class="fa fa-angle-down"></i>
                                    </a>
                                    <ul class="dropdown-submenu dropdown-hover">
                                        <li><a href="#">Makasar</a></li>
                                        <li><a href="#">Palu</a></li>
                                        <li><a href="#">Kendari</a></li>
                                        <li><a href="#">Manado</a></li>
                                        <li><a href="#">Gorontalo</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a class="" href="index.html">
                                        <span class="menu-text"> Bali & Lombok</span>
                                        <i class="fa fa-angle-down"></i>
                                    </a>
                                    <ul class="dropdown-submenu dropdown-hover">
                                        <li><a href="#">Bali</a></li>
                                        <li><a href="#">Denpasar</a></li>
                                        <li><a href="#">Lombok</a></li>
                                        <li><a href="#">Mataram</a></li>

                                    </ul>
                                </li>
                                <li>
                                    <a class="" href="index.html">
                                        <span class="menu-text"> Home</span>
                                        <i class="fa fa-angle-down"></i>
                                    </a>
                                    <ul class="dropdown-submenu dropdown-hover">
                                        <li><a href="contact-us.html">Contact</a></li>
                                        <li><a href="my-account.html">My Account</a></li>
                                        <li><a href="frequently-questions.html">FAQ</a></li>
                                        <li><a href="login.html">Login</a></li>
                                        <li><a href="register.html">Register</a></li>
                                    </ul>
                                </li>



                            </ul>
                        </nav>
                    </div>
                    <div class="col-lg-2 col-md-6 col-6 col-custom">
                        <div class="header-right-area main-nav">
                            <ul class="nav">
                                <li class="sidemenu-wrap">
                                    <a href="#"><i class="fa-solid fa-right-from-bracket"></i></a>
                                </li>
                                <li class="sidemenu-wrap">
                                    <a href="#"><i class="fa fa-search"></i> </a>
                                    <ul class="dropdown-sidemenu dropdown-hover-2 dropdown-search">
                                        <li>
                                            <form action="#">
                                                <input name="search" id="search" placeholder="Search" type="text" />
                                                <button type="submit">
                                                    <i class="fa fa-search"></i>
                                                </button>
                                            </form>
                                        </li>
                                    </ul>
                                </li>

                                <li class="sidemenu-wrap">
                                    <a href="#"><i class="fa-solid fa-user"></i></i> </a>
                                </li>
                                <li class="account-menu-wrap d-none d-lg-flex">
                                    <a href="#" class="off-canvas-menu-btn">
                                        <i class="fa fa-bars"></i>
                                    </a>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Main Header Area End -->
        <!-- off-canvas menu start -->
        <aside class="off-canvas-wrapper" id="mobileMenu">
            <div class="off-canvas-overlay"></div>
            <div class="off-canvas-inner-content">
                <div class="btn-close-off-canvas">
                    <i class="fa fa-times"></i>
                </div>
                <div class="off-canvas-inner">
                    <div class="search-box-offcanvas">
                        <form>
                            <input type="text" placeholder="Search product..." />
                            <button class="search-btn"><i class="fa fa-search"></i></button>
                        </form>
                    </div>
                    <!-- mobile menu start -->
                    <div class="mobile-navigation">
                        <!-- mobile menu navigation start -->
                        <nav>
                            <ul class="mobile-menu">
                                <li class="menu-item-has-children">
                                    <a href="#">Sumatera</a>

                                </li>
                                <li class="menu-item-has-children">
                                    <a href="#">Jawa</a>

                                </li>
                                <li class="menu-item-has-children">
                                    <a href="#">Blog</a>
                                    <ul class="dropdown">
                                        <li><a href="blog.html">Blog Left Sidebar</a></li>
                                        <li>
                                            <a href="blog-list-right-sidebar.html">Blog List Right Sidebar</a>
                                        </li>
                                        <li>
                                            <a href="blog-list-fullwidth.html">Blog List Fullwidth</a>
                                        </li>
                                        <li><a href="blog-grid.html">Blog Grid Page</a></li>
                                        <li>
                                            <a href="blog-grid-right-sidebar.html">Blog Grid Right Sidebar</a>
                                        </li>
                                        <li>
                                            <a href="blog-grid-fullwidth.html">Blog Grid Fullwidth</a>
                                        </li>
                                        <li>
                                            <a href="blog-details-sidebar.html">Blog Details Sidebar Page</a>
                                        </li>
                                        <li>
                                            <a href="blog-details-fullwidth.html">Blog Details Fullwidth Page</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children">
                                    <a href="#">Pages</a>
                                    <ul class="dropdown">
                                        <li><a href="frequently-questions.html">FAQ</a></li>
                                        <li><a href="my-account.html">My Account</a></li>
                                        <li>
                                            <a href="login-register.html">login &amp; register</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a href="about-us.html">About Us</a></li>
                                <li><a href="contact-us.html">Contact</a></li>
                            </ul>
                        </nav>
                        <!-- mobile menu navigation end -->
                    </div>
                    <!-- mobile menu end -->
                    <div class="offcanvas-widget-area">
                        <div class="switcher">
                            <div class="language">
                                <span class="switcher-title">Language: </span>
                                <div class="switcher-menu">
                                    <ul>
                                        <li>
                                            <a href="#">English</a>
                                            <ul class="switcher-dropdown">
                                                <li><a href="#">German</a></li>
                                                <li><a href="#">French</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="currency">
                                <span class="switcher-title">Currency: </span>
                                <div class="switcher-menu">
                                    <ul>
                                        <li>
                                            <a href="#">$ USD</a>
                                            <ul class="switcher-dropdown">
                                                <li><a href="#">€ EUR</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="top-info-wrap text-left text-black">
                            <ul class="address-info">
                                <li>
                                    <i class="fa fa-phone"></i>
                                    <a href="info%40yourdomain.html">(1245) 2456 012</a>
                                </li>
                                <li>
                                    <i class="fa fa-envelope"></i>
                                    <a href="info%40yourdomain.html">info@yourdomain.com</a>
                                </li>
                            </ul>
                            <div class="widget-social">
                                <a class="facebook-color-bg" title="Facebook-f" href="#"><i class="fa fa-facebook-f"></i></a>
                                <a class="twitter-color-bg" title="Twitter" href="#"><i class="fa fa-twitter"></i></a>
                                <a class="linkedin-color-bg" title="Linkedin" href="#"><i class="fa fa-linkedin"></i></a>
                                <a class="youtube-color-bg" title="Youtube" href="#"><i class="fa fa-youtube"></i></a>
                                <a class="vimeo-color-bg" title="Vimeo" href="#"><i class="fa fa-vimeo"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </aside>
        <!-- off-canvas menu end -->
        <!-- off-canvas menu start -->
        <aside class="off-canvas-menu-wrapper" id="sideMenu">

        </aside>
        <!-- off-canvas menu end -->
    </header>
