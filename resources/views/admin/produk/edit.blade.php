<!-- resources/views/admin/produk/edit.blade.php -->
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Formulir Edit Produk</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
</head>

<body class="container mt-5">
    <h1>Formulir Edit Produk</h1>

    @if($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <form action="{{ route('produk.update', $produk->id) }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')

        <div class="form-group">
            <label for="nama">Nama Produk:</label>
            <input type="text" class="form-control" name="nama" value="{{ $produk->nama }}" required>
        </div>

        <div class="form-group">
            <label for="harga">Harga:</label>
            <input type="number" class="form-control" name="harga" value="{{ $produk->harga }}" required>
        </div>

        <div class="form-group">
            <label for="deskripsi">Deskripsi:</label>
            <textarea class="form-control" name="deskripsi" required>{{ $produk->deskripsi }}</textarea>
        </div>

        <div class="form-group">
            <label for="gambar">Gambar:</label>
            @if($produk->gambar)
            <img src="{{ asset('storage/' . $produk->gambar) }}" alt="Gambar Produk" style="max-width: 100px;">
            @else
            Tidak Ada Gambar
            @endif
            <input type="file" class="form-control-file" name="gambar">
        </div>

        <div class="form-group">
            <label for="kategori_id">Kategori:</label>
            <select class="form-control" name="kategori_id" required>
                @foreach($kategoris as $kategori)
                <option value="{{ $kategori->id }}" {{ $produk->kategori_id == $kategori->id ? 'selected' : '' }}>{{ $kategori->nama }}</option>
                @endforeach
            </select>
        </div>

        <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
    </form>

    <a href="{{ route('produk.index') }}" class="btn btn-secondary mt-3">Kembali ke Daftar Produk</a>

    <!-- Optional: Add Bootstrap JS and Popper.js for Bootstrap's JavaScript plugins -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</body>

</html>