<!-- resources/views/admin/wilayah/create.blade.php -->

<h1>Formulir Tambah Wilayah</h1>

@if($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<form action="{{ route('wilayah.store') }}" method="POST">
    @csrf
    <label for="nama">Nama Wilayah:</label>
    <input type="text" name="nama" required>

    <label for="deskripsi">Deskripsi:</label>
    <textarea name="deskripsi" required></textarea>

    <button type="submit">Tambah Wilayah</button>
</form>

<a href="{{ route('wilayah.index') }}">Kembali ke Daftar Wilayah</a>