<!-- resources/views/admin/wilayah/edit.blade.php -->

<h1>Formulir Edit Wilayah</h1>

@if($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<form action="{{ route('wilayah.update', $wilayah->id) }}" method="POST">
    @csrf
    @method('PUT')
    <label for="nama">Nama Wilayah:</label>
    <input type="text" name="nama" value="{{ $wilayah->nama }}" required>

    <label for="deskripsi">Deskripsi:</label>
    <textarea name="deskripsi" required>{{ $wilayah->deskripsi }}</textarea>

    <button type="submit">Simpan Perubahan</button>
</form>

<a href="{{ route('wilayah.index') }}">Kembali ke Daftar Wilayah</a>