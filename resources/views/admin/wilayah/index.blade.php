<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Daftar Wilayah</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
</head>

<body class="container mt-5">

    <h1>Daftar Wilayah</h1>

    @if(session('success'))
    <div class="alert alert-success mt-3">
        {{ session('success') }}
    </div>
    @endif

    <table class="table table-bordered mt-3">
        <thead class="thead-dark">
            <tr>
                <th scope="col">No.</th>
                <th scope="col">Nama</th>
                <th scope="col">Deskripsi</th>
                <th scope="col">Aksi</th>
            </tr>
        </thead>
        <tbody>
            @forelse($wilayahs as $index => $wilayah)
            <tr>
                <td>{{ $index + 1 }}</td>
                <td>{{ $wilayah->nama }}</td>
                <td>{{ $wilayah->deskripsi }}</td>
                <td>
                    <a href="{{ route('wilayah.edit', $wilayah->id) }}" class="btn btn-primary btn-sm">Edit</a>
                    <form action="{{ route('wilayah.destroy', $wilayah->id) }}" method="POST" style="display: inline;">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Apakah Anda yakin ingin menghapus wilayah ini?')">Hapus</button>
                    </form>
                </td>
            </tr>
            @empty
            <tr>
                <td colspan="4" class="text-center">Tidak ada wilayah saat ini.</td>
            </tr>
            @endforelse
        </tbody>
    </table>

    <a href="{{ route('wilayah.create') }}" class="btn btn-success mt-3">Tambah Wilayah Baru</a>

</body>

</html>