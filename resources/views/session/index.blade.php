@extends('layout.master')
@section('content')

<body>
    <div class="breadcrumbs-area position-relative">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="breadcrumb-content position-relative section-content">
                        <h3 class="title-3">Login-Register</h3>
                        <ul>
                            <li><a href="index.html">Home</a></li>
                            <li>Login-Register</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="login-register-area mt-no-text">
        <div class="container custom-area">
            <div class="row">
                <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2 col-custom">
                    <div class="login-register-wrapper">
                        <div class="section-content text-center mb-5">
                            <h2 class="title-4 mb-2">Login</h2>
                            <p class="desc-content">Please login using account detail bellow.</p>
                        </div>
                        <form action="/session/login" method="POST">
                            @csrf
                            <div class="single-input-item mb-3">
                                <label for="email">Email</label>
                                <input type="email" name="email" class="form-control" placeholder="Enter your Email">
                            </div>
                            @error('email')
                            <div class="alert alert-danger ">{{ $message }}</div>
                            @enderror
                            <div class="single-input-item mb-3">
                                <label for="password">Password</label>
                                <input type="password" name="password" class="form-control" placeholder="Enter your Password">
                            </div>
                            @error('password')
                            <div class="alert alert-danger ">{{ $message }}</div>
                            @enderror

                            <div class="single-input-item mb-3">
                                <button class="btn flosun-button secondary-btn theme-color rounded-0">Login</button>
                            </div>
                            <div class="single-input-item">
                                <a href="register.html">Create Account</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
@endsection