<?php

// app/Http/Controllers/ProdukController.php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Models\Produk;
use App\Models\Kategori;
use App\Models\Wilayah;

class ProdukController extends Controller
{
    public function index()
    {
        $produks = Produk::all();
        return view('admin.produk.index', compact('produks'));
    }

    public function create()
    {
        $kategoris = Kategori::all();
        $wilayahs = Wilayah::all();

        // Kirim data ke tampilan create.blade.php
        return view('admin.produk.create', [
            'kategoris' => $kategoris,
            'wilayahs' => $wilayahs,
        ]);
    }


    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|string',
            'harga' => 'required|numeric',
            'deskripsi' => 'required|string',
            'gambar' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'kategori_id' => 'required|exists:kategoris,id',
            'wilayah_id' => 'required|exists:wilayahs,id',
        ]);

        // Upload gambar jika ada
        $gambarPath = null;
        if ($request->hasFile('gambar')) {
            $gambarPath = $request->file('gambar')->store('produk_images', 'public');
        }

        Produk::create([
            'nama' => $request->nama,
            'harga' => $request->harga,
            'deskripsi' => $request->deskripsi,
            'gambar' => $gambarPath,
            'kategori_id' => $request->kategori_id,
            'wilayah_id' => $request->wilayah_id,
        ]);

        return redirect()->route('produk.index')->with('success', 'Produk berhasil ditambahkan.');
    }
    public function edit($id)
    {
        $produk = Produk::findOrFail($id);
        $kategoris = Kategori::all();
        return view('admin.produk.edit', compact('produk', 'kategoris'));
    }

    // Menyimpan perubahan pada produk
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required|string',
            'harga' => 'required|numeric',
            'deskripsi' => 'required|string',
            'kategori_id' => 'required|exists:kategoris,id',
            'wilayah_id' => 'required|exists:wilayahs,id',
            'gambar' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $produk = Produk::findOrFail($id);

        // Handle gambar jika diubah
        if ($request->hasFile('gambar')) {
            // Hapus gambar lama jika ada
            if ($produk->gambar) {
                unlink(storage_path('app/public/' . $produk->gambar));
            }

            // Upload gambar baru
            $gambarPath = $request->file('gambar')->store('public/produk');
            $produk->gambar = str_replace('public/', '', $gambarPath);
        }

        // Update data produk
        $produk->update([
            'nama' => $request->nama,
            'harga' => $request->harga,
            'deskripsi' => $request->deskripsi,
            'kategori_id' => $request->kategori_id,
            'wilayah_id' => $request->wilayah_id,
        ]);

        return redirect()->route('produk.index')->with('success', 'Produk berhasil diperbarui.');
    }
    public function destroy($id)
    {
        $produk = Produk::findOrFail($id);

        // Hapus gambar terkait jika ada
        if ($produk->gambar) {
            Storage::delete('public/' . $produk->gambar);
        }

        $produk->delete();

        return redirect()->route('produk.index')->with('success', 'Produk berhasil dihapus.');
    }
}
