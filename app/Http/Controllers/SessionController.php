<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use SebastianBergmann\CodeUnit\FunctionUnit;
use Symfony\Component\Console\Input\Input;
use App\Http\Controllers\DashboardController;

class SessionController extends Controller
{
    public function index()
    {
        return view('session.index');
    }
    function login(Request $request)
    {
        // Session::flash('email', $request->email);
        $request->validate([
            'email' => 'required',
            'password' => 'required'
        ], [
            'email.required' => 'Email wajib disi',
            'password.required' => 'Password tidak boleh kosong',
        ]);
        $infologin = [
            'email' => $request->email,
            'password' => $request->password
        ];
        if (Auth::attempt($infologin)) {
            //kalau otentifikasi asukses
            return redirect('/')->with('success', 'username dan password yang dimasukkan tidak valid');
        } else {
            return 'gagal login';
        }
        function logout()
        {
            Auth::logout();
            return redirect('session')->with('success', 'berhasil logout');
        }
    }
}
