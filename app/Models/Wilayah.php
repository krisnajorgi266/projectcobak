<?php

namespace App\Models;

use App\Http\Controllers\AdminController;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Wilayah extends Model
{
    protected $fillable = ['nama', 'deskripsi'];
    
    public function wilayah()
    {
        return $this->belongsTo(Wilayah::class);
    }
}
